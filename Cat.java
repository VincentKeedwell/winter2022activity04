public class Cat
{
	private int age;
	public String name;
	public String colours;
	public String attitude;
	
	public void sayHi()
	{
		System.out.println("Hi "+this.name);
	}
	
	public void attitudeChecker()
	{
		if (this.attitude.equals("Calm"))
		{
			System.out.println("My Cat Windy Is Calm Too!");
		}
		else if (this.attitude.equals("Energetic"))
		{
			System.out.println("My Cat Thunder Is Energetic Too!");
		}
		else
		{
			System.out.println("My Cats are not "+this.attitude);
		}
	}
	
	public int getAge() {
		return this.age;
	}
	public String getName() {
		return this.name;
	}
	public String getColours() {
		return this.colours;
	}
	public String getAttitude() {
		return this.attitude;
	}
	
	public void setAge(int newAge) {
		this.age = newAge;
	}
	
	public Cat(String name, String colours, String attitude)
	{
		this.age = 1;
		this.name = name;
		this.colours = colours;
		this.attitude = attitude;
	}
	
	public void growUp()
	{
		this.age = this.age + 1;
	}
}