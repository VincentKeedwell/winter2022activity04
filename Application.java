public class Application
{
	public static void main(String[] args)
	{
		Cat tux = new Cat("Default", "White", "Default");
		Cat mittens = new Cat("Default", "White", "Default");
		
		tux.setAge(2);
		System.out.println("Age: "+tux.getAge());
		System.out.println("Name: "+tux.getName());
		System.out.println("Colours: "+tux.getColours());
		System.out.println("Attitude: "+tux.getAttitude());
		
		mittens.setAge(5);
		System.out.println("Age: "+mittens.getAge());
		System.out.println("Name: "+mittens.getName());
		System.out.println("Colours: "+mittens.getColours());
		System.out.println("Attitude: "+mittens.getAttitude());
		
		System.out.println(" ");
		tux.sayHi();
		mittens.sayHi();
		
		Cat[] clowder = new Cat[3];
		clowder[0] = tux;
		clowder[1] = mittens;
		System.out.println(clowder[0].getName());
		System.out.println(clowder[2].getName());
	}
}